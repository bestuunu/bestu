from django.db import models

# Create your models here.

class HealthStatus(models.Model):
	health_level = models.IntegerField(default=1)
	exercise_count = models.IntegerField(default=0)
	heart_rate = models.IntegerField(default=72)
	steps = models.IntegerField(default=0)
	calories = models.IntegerField(default=0)

class Skills(models.Model):
	skill_name = models.CharField(max_length=200)
	skill_level = models.IntegerField(default=0)
	skill_field = models.CharField(max_length=200)

class Goals(models.Model):
	goal_name = models.CharField(max_length=200)
	requirement = models.ForeignKey(Skills, on_delete=models.CASCADE)
	goal_progress = models.IntegerField(default=0)

class Person(models.Model):
	name = models.CharField(max_length=200)
	age = models.IntegerField(default=18)
	job_title = models.CharField(max_length=200)
	healthStatus = models.ForeignKey(HealthStatus, on_delete=models.CASCADE)
	skills = models.ForeignKey(Skills, on_delete=models.CASCADE)
	goals = models.ForeignKey(Goals, on_delete=models.CASCADE)
	# models.DateTimeField('date published')