# Level Up! Readme

# This is the code for our life coach management app 'Level Up!' developed as part of the Mission Hack hackathon.

# The goal is to enable people to better achieve self-actualization in four key areas: health, personal, education, and career development.
