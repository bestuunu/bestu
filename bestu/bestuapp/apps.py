from django.apps import AppConfig


class BestuappConfig(AppConfig):
    name = 'bestuapp'
